#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int main(){
	char fishStatus;
	int fishing;
        const char * arrayOfFish[3];
        arrayOfFish[0] = "Bass";
	arrayOfFish[1] = "Mackerel";
	arrayOfFish[2] = "Whiting";

	srand(time(NULL));

	printf("Type f to fish.\n");
	scanf(" %c", &fishStatus);

	if (fishStatus == 'f'){
	   printf("You begin fishing...\n");

	   fishing = rand() % 9 + 3;
	   sleep(fishing);
	}else{
	   printf("Wrong input!\n");
	   exit(0);
	}

	const char * random;
	random = arrayOfFish[rand() % 3];
	printf("You caught a: %s\n", random);

	printf("\nFished for %d seconds.\n", fishing);

    return 0;
}